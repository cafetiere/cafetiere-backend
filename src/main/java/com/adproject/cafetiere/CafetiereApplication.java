package com.adproject.cafetiere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CafetiereApplication {

    public static void main(String[] args) {
        SpringApplication.run(CafetiereApplication.class, args);
    }

}
