package com.adproject.cafetiere.authentication.controller;

import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.authentication.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @PostMapping(path = "/register", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity registerAccount(@RequestBody Account account) {
        return ResponseEntity.ok(authService.register(account));
    }
}
