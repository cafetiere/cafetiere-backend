package com.adproject.cafetiere.authentication.repository;

import com.adproject.cafetiere.authentication.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends JpaRepository<Account, Integer> {
    Account findAccountByUsername(String username);

    Account findAccountById(int id);
}
