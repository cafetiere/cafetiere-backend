package com.adproject.cafetiere.authentication.service;

import com.adproject.cafetiere.authentication.model.Account;

public interface AuthService {
    Account register(Account account);

    Account getAccountById(int id);
}
