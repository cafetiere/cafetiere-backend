package com.adproject.cafetiere.authentication.service;

import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.authentication.repository.AuthRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    private AuthRepository authRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public Account register(Account account) {
        Account byUsername = authRepository.findAccountByUsername(account.getUsername());
        if (byUsername != null) {
            throw new RuntimeException("Username already exist, use another username.");
        }
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        authRepository.save(account);
        return account;
    }

    @Override
    public Account getAccountById(int id) {
        return authRepository.findAccountById(id);
    }
}
