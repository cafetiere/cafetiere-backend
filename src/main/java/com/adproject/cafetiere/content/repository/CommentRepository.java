package com.adproject.cafetiere.content.repository;

import com.adproject.cafetiere.content.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {
    Comment findCommentByIdComment(Integer idComment);

    List<Comment> findCommentsByPostIdPostOrderByIdCommentDesc(int postId);
}
