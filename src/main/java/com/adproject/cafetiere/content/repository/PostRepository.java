package com.adproject.cafetiere.content.repository;

import com.adproject.cafetiere.content.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface PostRepository extends JpaRepository<Post, Integer> {
    Post findPostByIdPost(Integer idPost);

    List<Post> findByTitlePostContainingIgnoreCase(String keyword);

    List<Post> findAllByUpdatedTimeBeforeOrderByUpdatedTimeDesc(Date time);
}
