package com.adproject.cafetiere.content.controller;

import com.adproject.cafetiere.content.model.Comment;
import com.adproject.cafetiere.content.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping(path = "/accountId={idAccount}/postId={idPost}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Comment> createComment(@RequestBody Comment comment,
                                                 @PathVariable(name = "idAccount") int idAccount,
                                                 @PathVariable(name = "idPost") int idPost) {
        return ResponseEntity.ok(commentService.createComment(comment, idAccount, idPost));
    }

    @GetMapping(path = "/postId={idPost}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Comment>> getCommentFromPost(
            @PathVariable(name = "idPost") int idPost) {
        return ResponseEntity.ok(commentService.getCommentByPostId(idPost));
    }

    @GetMapping(path = "/commentId={idComment}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Comment> getCommentByIdComment(
            @PathVariable(name = "idComment") int idComment) {
        return ResponseEntity.ok(commentService.getCommentByCommentId(idComment));
    }

    @DeleteMapping(path = "/accountId={accountId}/commentId={idComment}",
            produces = {"application/json"})
    public ResponseEntity<Comment> deleteCommentById(
            @PathVariable(value = "accountId") int accountId,
            @PathVariable(value = "idComment") int commentId) {
        commentService.deleteCommentById(accountId, commentId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
