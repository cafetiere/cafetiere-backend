package com.adproject.cafetiere.content.controller;

import com.adproject.cafetiere.content.model.Post;
import com.adproject.cafetiere.content.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/post")
public class PostController {

    @Autowired
    private PostService postService;

    @PostMapping(path = "/accountId={id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Post> createPost(@RequestBody Post post,
                                           @PathVariable(name = "id") int id) {
        return ResponseEntity.ok(postService.createPost(post, id));
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Post>> getAllPosts() {
        return ResponseEntity.ok(postService.getAllPosts());
    }


    @GetMapping(path = "/postId={postId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Post> getPostByIdPost(@PathVariable(value = "postId") int id) {
        Post post = postService.getPostByIdPost(id);
        return ResponseEntity.ok(post);
    }

    @GetMapping(path = "/containTitle={keyword}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Post>> getPostByKeyword(
            @PathVariable(value = "keyword") String keyword) {
        return ResponseEntity.ok(postService.getAllPostsContainsKeyword(keyword));
    }

    @PutMapping(path = "/accountId={accountId}/postId={postId}", produces = {"application/json"})
    public ResponseEntity<Post> updatePostById(@PathVariable(value = "accountId") int accountId,
                                               @PathVariable(value = "postId") int postId,
                                               @RequestBody Post post) {
        return ResponseEntity.ok(postService.updatePostById(post, postId, accountId));
    }

    @DeleteMapping(path = "/accountId={accountId}/postId={postId}", produces = {"application/json"})
    public ResponseEntity<Post> deletePostById(@PathVariable(value = "accountId") int accountId,
                                               @PathVariable(value = "postId") int postId) {
        postService.deletePostById(postId, accountId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
