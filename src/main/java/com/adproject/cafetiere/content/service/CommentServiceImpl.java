package com.adproject.cafetiere.content.service;

import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.authentication.service.AuthService;
import com.adproject.cafetiere.content.model.Comment;
import com.adproject.cafetiere.content.model.Post;
import com.adproject.cafetiere.content.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostService postService;

    @Autowired
    private AuthService authService;

    /*
    Method createComment digunakan untuk membuat sebuah comment dan setiap user bisa
    membuat baik pembeli ataupun admin. Setelah comment dibuat maka comment tersebut akan
    diset ke suatu post dan suatu user setelah itu baru disave di commentrepository.
    */
    @Override
    public Comment createComment(Comment comment, int idAccount, int idPost) {
        Account account = authService.getAccountById(idAccount);
        Post post = postService.getPostByIdPost(idPost);
        comment.setPost(post);
        comment.setUser(account);
        commentRepository.save(comment);
        return comment;
    }

    /*
    Method getCommentByCommentId digunakan untuk mendapatkan suatu comment dari commentRepository
    berdasarkan idComment.
     */
    @Override
    public Comment getCommentByCommentId(int idComment) {
        return commentRepository.findCommentByIdComment(idComment);
    }

    /*
    Method getCommentByPostId digunakan untuk mendapatkan kumpulan comment di dalam suatu post
    (yang postnya diambil dari id nya) dan commentnya di sort berdasarkan id commentnya.
     */
    @Override
    public List<Comment> getCommentByPostId(int idPost) {
        return commentRepository.findCommentsByPostIdPostOrderByIdCommentDesc(idPost);
    }

    /*
    Method deleteCommendById digunakan untuk menghapus suatu comment dari idCommentnya.
    Yang bisa menghapusnya hanyalah yang memiliki role ADMIN atau buyer terkait yang
    mengpost comment tersebut.
     */
    @Override
    public void deleteCommentById(int idAccount, int idComment) {
        Account account = authService.getAccountById(idAccount);
        Comment comment = commentRepository.findCommentByIdComment(idComment);
        if (account.getRole() == 1) {
            commentRepository.deleteById(idComment);
        } else if (account.getRole() == 0 && comment.getUser() == account) {
            commentRepository.deleteById(idComment);
        }
    }


}
