package com.adproject.cafetiere.content.service;

import com.adproject.cafetiere.content.model.Comment;

import java.util.List;

public interface CommentService {
    Comment createComment(Comment comment, int idAccount, int idPost);

    Comment getCommentByCommentId(int idComment);

    List<Comment> getCommentByPostId(int idPost);

    void deleteCommentById(int idAccount, int idComment);
}
