package com.adproject.cafetiere.content.service;

import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.authentication.service.AuthService;
import com.adproject.cafetiere.content.model.Comment;
import com.adproject.cafetiere.content.model.Post;
import com.adproject.cafetiere.content.repository.PostRepository;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private AuthService authService;

    @Autowired
    private CommentService commentService;

    /*
    Method createPost ini digunakan untuk membuat suatu post apabila account yang diget dari
    idAccountnya memiliki role ADMIN lalu post yang dibuat akan di set memiliki sebuah admin
    dan postnya di save di post repo
     */
    @Override
    public Post createPost(Post post, int idAccount) {
        Account account = authService.getAccountById(idAccount);
        if (account.getRole() == 1) {
            post.setAdmin(account);
            postRepository.save(post);
            return post;
        }
        return null;
    }

    /*
    Method getPostByIdPost ini digunakan untuk mendapatkan suatu post berdasarkan idPostnya
     */
    @Override
    public Post getPostByIdPost(Integer idPost) {
        return postRepository.findPostByIdPost(idPost);
    }

    /*
    Method getAllPostsContainsKeyword ini digunakan untuk mendapatkan suatu kumpulan post
    yang memiliki suatu keyword di titlenya
     */
    @Override
    public Iterable<Post> getAllPostsContainsKeyword(String keyword) {
        return postRepository.findByTitlePostContainingIgnoreCase(keyword);
    }

    /*
    Method getAllPosts akan mendapatkan kumpulan post yang ada
     */
    @Override
    public Iterable<Post> getAllPosts() {
        return postRepository.findAllByUpdatedTimeBeforeOrderByUpdatedTimeDesc(new Date());
    }

    /*
    Method updatePostById ini digunakan untuk mengupdate post jika dan hanya jika post tersebut
    dipost oleh account yang memiliki role ADMIN dan account yang terkait yang post diawal.
    Lalu post yang terupdate akan memiliki kumpulan comment yang ada di post lama, memasukkan
    last update time, memasukkan admin, dan save di post repo
     */
    @Override
    public Post updatePostById(Post post, int idPost, int idAccount) {
        Post currentPost = postRepository.findPostByIdPost(idPost);
        if (currentPost.getAdmin().getId() == idAccount) {
            post.setIdPost(idPost);
            Account admin = currentPost.getAdmin();
            List<Comment> listOfComment = currentPost.getListOfComment();
            post.setUpdatedTime(new Date());
            post.setListOfComment(listOfComment);
            post.setAdmin(admin);
            postRepository.save(post);
            return post;
        }

        return null;
    }


    /*
    Method deletePostById ini digunakan untuk menghapus suatu post berdasarkan idPostnya dan
    yang bisa menghapus adalah account yang memiliki role ADMIN dan harus account yang terkait
    yang post diawal. Apabila post dihapus maka, kita akan hapus comment-comment di post tersebut.
     */
    @Override
    public void deletePostById(int idPost, int idAccount) {
        Account account = authService.getAccountById(idAccount);
        Post post = postRepository.findPostByIdPost(idPost);
        if (account.getRole() == 1 && post.getAdmin().getId() == idAccount) {
            for (Comment comment : post.getListOfComment()) {
                commentService.deleteCommentById(idAccount, comment.getIdComment());
            }
            postRepository.deleteById(idPost);
        }

    }
}
