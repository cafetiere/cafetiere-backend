package com.adproject.cafetiere.content.service;

import com.adproject.cafetiere.content.model.Post;

public interface PostService {
    Post createPost(Post post, int idAccount);

    Post getPostByIdPost(Integer idPost);

    Iterable<Post> getAllPostsContainsKeyword(String keyword);

    Iterable<Post> getAllPosts();

    Post updatePostById(Post post, int idPost, int idAccount);

    void deletePostById(int idPost, int idAccount);
}
