package com.adproject.cafetiere.content.model;

import com.adproject.cafetiere.authentication.model.Account;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.util.Date;

@Data
@Entity
@Table(name = "comment")
@NoArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_comment", updatable = false, nullable = false)
    private int idComment;

    @Column(name = "isi_comment")
    private String isiComment;

    @Column(name = "`timestamp`")
    @CreationTimestamp
    private Date publishedTime;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private Date updatedTime;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_post", nullable = false)
    private Post post;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Account user;

    /**
     * Constructor Comment.
     */
    public Comment(String isiComment, Date publishedTime) {
        this.isiComment = isiComment;
        this.publishedTime = publishedTime;
    }
}
