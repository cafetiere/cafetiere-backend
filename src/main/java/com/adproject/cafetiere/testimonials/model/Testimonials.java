package com.adproject.cafetiere.testimonials.model;

import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.product.model.Product;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "testimonials")
@Data
@NoArgsConstructor
public class Testimonials {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "harga", nullable = false)
    private int harga;

    @Column(name = "rasa", nullable = false)
    private String rasa;

    @Column(name = "pelayanan", nullable = false)
    private String pelayanan;

    @ManyToOne(optional = false)
    @PrimaryKeyJoinColumn(name = "account")
    @JsonIgnoreProperties("testimonials")
    private Account account;

    @ManyToOne(optional = false)
    @PrimaryKeyJoinColumn(name = "product")
    @JsonIgnoreProperties("testimonials")
    private Product product;

    /**
     * Constructor testimonials.
     */
    public Testimonials(Account account, Product product,
                        int harga, String rasa, String pelayanan) {
        this.account = account;
        this.product = product;
        this.harga = harga;
        this.rasa = rasa;
        this.pelayanan = pelayanan;
    }
}
