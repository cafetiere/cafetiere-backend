package com.adproject.cafetiere.testimonials.controller;

import com.adproject.cafetiere.testimonials.model.Testimonials;
import com.adproject.cafetiere.testimonials.service.TestimonialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/testi")
public class TestimonialsController {
    @Autowired
    public TestimonialsService testimonialsService;

    /**
     * Controller untuk handle request membuat testimonials.
     */
    @PostMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Testimonials> createTestimonial(
            @PathVariable(value = "id") int id,
            @RequestBody Testimonials testimonials) {
        return ResponseEntity.ok(testimonialsService.createTestimonials(id, testimonials));
    }

    /**
     * Controller untuk handle request menampilkan semua testimonials.
     */
    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<List<Testimonials>> getAllTestimonial() {
        return ResponseEntity.ok(testimonialsService.getAllOfTestimonials());
    }

    /**
     * Controller untuk handle request menampilkan list testimonials berdasarkan id.
     */
    @GetMapping(path = "/{id}/list-testi", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<List<Testimonials>> getListTestimonials(
            @PathVariable(value = "id") int id) {
        List<Testimonials> testimonials = testimonialsService.getListTestimonialsById(id);
        if (testimonials == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(testimonials);
    }

    /**
     * Controller untuk handle request menampilkan testimonials berdasarkan id.
     */
    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Testimonials> getTestimonials(@PathVariable(value = "id") int id) {
        Testimonials testimonials = testimonialsService.getTestimonialsById(id);
        if (testimonials == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(testimonials);
    }

}
