package com.adproject.cafetiere.testimonials.service;

import com.adproject.cafetiere.testimonials.model.Testimonials;

import java.util.List;

public interface TestimonialsService {
    Testimonials createTestimonials(int id, Testimonials testimonials);

    Testimonials getTestimonialsById(int id);

    List<Testimonials>  getListTestimonialsById(int id);

    List<Testimonials> getAllOfTestimonials();
}
