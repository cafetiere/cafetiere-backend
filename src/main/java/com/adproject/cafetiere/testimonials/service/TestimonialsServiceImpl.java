package com.adproject.cafetiere.testimonials.service;

import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.authentication.repository.AuthRepository;
import com.adproject.cafetiere.product.model.Product;
import com.adproject.cafetiere.product.repository.ProductRepository;
import com.adproject.cafetiere.testimonials.model.Testimonials;
import com.adproject.cafetiere.testimonials.repository.TestimonialsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestimonialsServiceImpl implements TestimonialsService {
    @Autowired
    public TestimonialsRepository testimonialsRepository;

    @Autowired
    public AuthRepository authRepository;

    @Autowired
    public ProductRepository productRepository;

    /**
     * Method untuk membuat testimonials.
     */
    @Override
    public Testimonials createTestimonials(int id, Testimonials testimonials) {
        Product product = productRepository.findById(id);
        Account auth = authRepository.findAccountById(id);
        if (product != null && auth != null) {
            testimonials.setProduct(product);
            testimonials.setAccount(auth);
            testimonialsRepository.save(testimonials);
        }
        return testimonials;
    }

    /**
     * Method untuk mengambil testimonials berdasarkan id.
     */
    @Override
    public Testimonials getTestimonialsById(int id) {
        return testimonialsRepository.findById(id);
    }

    /**
     * Method untuk mendapatkan semua testimonials.
     */
    @Override
    public List<Testimonials> getAllOfTestimonials() {
        return testimonialsRepository.findAll();
    }

    /**
     * Method untuk mendapatkan list testimonials berdasarkan id.
     */
    @Override
    public List<Testimonials> getListTestimonialsById(int id) {
        Product product = productRepository.findById(id);
        if (product == null) {
            return null;
        }
        return testimonialsRepository.findByProduct(product);
    }

}
