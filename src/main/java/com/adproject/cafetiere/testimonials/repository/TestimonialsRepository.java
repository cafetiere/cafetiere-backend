package com.adproject.cafetiere.testimonials.repository;

import com.adproject.cafetiere.product.model.Product;
import com.adproject.cafetiere.testimonials.model.Testimonials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestimonialsRepository extends JpaRepository<Testimonials, Integer> {
    Testimonials findById(int id);

    List<Testimonials> findByProduct(Product product);
}
