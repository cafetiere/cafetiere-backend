package com.adproject.cafetiere.product.service;

import com.adproject.cafetiere.product.model.Product;

public interface ProductService {
    Product createProduct(Product product);

    Iterable<Product> getListProduct();

    Product getProductById(int id);

    Product updateProduct(int id, Product product);

    void deleteProductById(int id);
}
