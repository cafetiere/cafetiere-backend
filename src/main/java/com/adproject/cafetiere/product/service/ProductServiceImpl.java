package com.adproject.cafetiere.product.service;

import com.adproject.cafetiere.product.model.Product;
import com.adproject.cafetiere.product.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product createProduct(Product product) {
        if (productRepository.findById(product.getId()) == null) {
            productRepository.save(product);
        }
        return product;
    }

    @Override
    public Iterable<Product> getListProduct() {
        return productRepository.findAll();
    }

    @Override
    public Product getProductById(int id) {
        return productRepository.findById(id);
    }

    @Override
    public Product updateProduct(int id, Product product) {
        Product existingProduct = productRepository.findById(id);
        if (existingProduct != null) {
            product.setId(id);
            productRepository.save(product);
            return product;
        }
        return null;
    }

    @Override
    public void deleteProductById(int id) {
        Product product = productRepository.findById(id);
        if (product != null) {
            productRepository.deleteById(id);
        }
    }
}
