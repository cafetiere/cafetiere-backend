package com.adproject.cafetiere.testimonials.model;


import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.product.model.Product;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestimonialsTest {
    private Testimonials testimonials = new Testimonials(
            new Account("hasna", "nadifah", "haha@gmail.com", "hnadifahsn", "huu", 1),
            new Product("latte", "enak", 10000, 1), 15000, "enak", "kurang");

    @Test
    void testSetAccount() {
        testimonials.setAccount(
                new Account("hasna", "nadifah", "haha@gmail.com", "hnadifahsn", "huu", 1));
        assertEquals(new Account("hasna", "nadifah", "haha@gmail.com", "hnadifahsn", "huu", 1),
                testimonials.getAccount());
    }

    @Test
    void testGetAccount() {
        assertEquals(new Account("hasna", "nadifah", "haha@gmail.com", "hnadifahsn", "huu", 1),
                testimonials.getAccount());
    }

    @Test
    void testSetProduct() {
        testimonials.setProduct(new Product("latte", "enak", 10000, 1));
    }

    @Test
    void testGetProduct() {
        assertEquals(new Product("latte", "enak", 10000, 1), testimonials.getProduct());
    }

    @Test
    void testSetHarga() {
        testimonials.setHarga(15000);
        assertEquals(15000, testimonials.getHarga());
    }

    @Test
    void testGetHarga() {
        assertEquals(15000, testimonials.getHarga());
    }

    @Test
    void testSetRasa() {
        testimonials.setRasa("enak");
        assertEquals("enak", testimonials.getRasa());
    }

    @Test
    void testGetRasa() {
        assertEquals("enak", testimonials.getRasa());
    }

    @Test
    void testSetPelayanan() {
        testimonials.setPelayanan("kurang");
        assertEquals("kurang", testimonials.getPelayanan());
    }

    @Test
    void testGetPelayanan() {
        assertEquals("kurang", testimonials.getPelayanan());
    }

}
