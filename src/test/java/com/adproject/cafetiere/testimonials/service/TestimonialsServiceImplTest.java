package com.adproject.cafetiere.testimonials.service;

import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.authentication.repository.AuthRepository;
import com.adproject.cafetiere.product.model.Product;
import com.adproject.cafetiere.product.repository.ProductRepository;
import com.adproject.cafetiere.testimonials.model.Testimonials;
import com.adproject.cafetiere.testimonials.repository.TestimonialsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;


@ExtendWith(MockitoExtension.class)
class TestimonialsServiceImplTest {
    @Mock
    private AuthRepository authRepository;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private TestimonialsRepository testimonialsRepository;

    @InjectMocks
    private TestimonialsServiceImpl testimonialsService;

    private Testimonials testimonials;
    private Product product;
    private Account account;

    @BeforeEach
    void setUp() {
        product = new Product("latte", "enak", 10000, 1);
        productRepository.save(product);
        account = new Account("hasna", "nadifah", "haha@gmail.com", "hnadifahsn", "huu", 1);
        authRepository.save(account);
        testimonials = new Testimonials(account, product, 16000, "enak", "kurang");
    }

    @Test
    void testGetAllOfTestimonials() {
        List<Testimonials> testimonialsList = testimonialsService.getAllOfTestimonials();
        lenient().when(testimonialsService.getAllOfTestimonials()).thenReturn(testimonialsList);
    }

    @Test
    void testCreateTestimonials() {
        Testimonials dummy;
        lenient().when(productRepository.findById(product.getId())).thenReturn(product);
        dummy = testimonialsService.createTestimonials(product.getId(), testimonials);
        assertNotEquals(null, dummy);
    }

    @Test
    void testGetTestimonialsById() {
        lenient().when(testimonialsService.getTestimonialsById(testimonials.getId()))
                .thenReturn(testimonials);
        Testimonials testimonialsResult = testimonialsService.getTestimonialsById(
                testimonials.getId());
        assertEquals(testimonials.getId(), testimonialsResult.getId());
    }

    @Test
    void testGetListTestimonialsById() {
        List<Testimonials> listTestimonials = testimonialsRepository.findByProduct(product);
        lenient().when(productRepository.findById(product.getId())).thenReturn(product);
        lenient().when(testimonialsRepository.findByProduct(product))
                .thenReturn(listTestimonials);
        List<Testimonials> listTestimonialsResult = testimonialsService.getListTestimonialsById(
                product.getId());
        assertEquals(listTestimonials, listTestimonialsResult);
    }
}
