package com.adproject.cafetiere.testimonials.controller;

import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.product.model.Product;
import com.adproject.cafetiere.testimonials.model.Testimonials;
import com.adproject.cafetiere.testimonials.service.TestimonialsServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = TestimonialsController.class)
class TestimonialsControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private TestimonialsServiceImpl testimonialsService;

    private Product product;
    private Testimonials testimonials;

    @BeforeEach
    void setUp() {
        product = new Product("latte", "enak", 10000, 1);
        testimonials = new Testimonials(
                new Account("hasna", "nadifah", "haha@gmail.com", "hnadifahsn", "huu", 1),
                product, 16000, "enak", "kurs");
        testimonials.setId(1);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerPostTestimonials() throws Exception {
        when(testimonialsService.createTestimonials(1, testimonials))
                .thenReturn(testimonials);
        mvc.perform(post("/testi/1").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(testimonials))).andExpect(jsonPath("$.id").value(1));
    }

    @Test
    void testControllerGetListTestimonials() throws Exception {
        List<Testimonials> listTesti = Arrays.asList(testimonials);
        when(testimonialsService.getAllOfTestimonials())
                .thenReturn(listTesti);
        mvc.perform(get("/testi").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value("1"));
    }

    @Test
    void testControllerGetTestiById() throws Exception {
        when(testimonialsService.getTestimonialsById(testimonials.getId()))
                .thenReturn(testimonials);
        mvc.perform(get("/testi/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(testimonials.getId()));
    }

    @Test
    void testControllerGetListTestiById() throws Exception {
        ArrayList testimonialsList = new ArrayList<Testimonials>();
        testimonialsList.add(testimonials);
        when(testimonialsService.getListTestimonialsById(product.getId()))
                .thenReturn(testimonialsList);
        mvc.perform(get("/testi/1/list-testi")).andExpect(status().isOk());
    }


}
