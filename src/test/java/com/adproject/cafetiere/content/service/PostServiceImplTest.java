package com.adproject.cafetiere.content.service;

import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.authentication.repository.AuthRepository;
import com.adproject.cafetiere.authentication.service.AuthService;
import com.adproject.cafetiere.content.model.Comment;
import com.adproject.cafetiere.content.model.Post;
import com.adproject.cafetiere.content.repository.CommentRepository;
import com.adproject.cafetiere.content.repository.PostRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class PostServiceImplTest {

    @Mock
    private AuthRepository accountRepository;

    @Mock
    private PostRepository postRepository;

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private AuthService accountService;

    @Mock
    private CommentService commentService;

    @InjectMocks
    private PostServiceImpl postService;

    private Post post;
    private Comment comment;
    private Account account;

    @BeforeEach
    public void setUp() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021, 1, 1, 0, 0, 0);
        Date publishedTime = calendar.getTime();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        post = new Post("TestTitle", "TestContent", publishedTime);

        calendar.set(2020, 1, 1, 1, 0, 0);
        Date publishedTime2 = calendar.getTime();
        comment = new Comment("TestIsiComment", publishedTime2);

        account = new Account("TestNamaDepan",
                "TestNamaBelakang",
                "TestEmail@mail.com",
                "TestUsername",
                "TestPassword",
                1);
        account.setId(0);

        post.setAdmin(account);
        comment.setPost(post);

        List<Comment> commentList = new ArrayList<>();
        commentList.add(comment);
        post.setListOfComment(commentList);
    }

    @Test
    void testServiceToCreatePost() {
        when(accountService.getAccountById(account.getId())).thenReturn(account);
        lenient().when(postService.createPost(post, account.getId())).thenReturn(post);
        assertEquals(postService.createPost(post, account.getId()), post);
        verify(postRepository).save(post);
    }

    @Test
    void testServiceGetPostByIdPost() {
        lenient().when(postService.getPostByIdPost(post.getIdPost())).thenReturn(post);
        assertEquals(post, postService.getPostByIdPost(post.getIdPost()));
    }

    @Test
    void testServiceUpdatePost() {
        when(accountService.getAccountById(account.getId())).thenReturn(account);

        postService.createPost(post, account.getId());

        lenient().when(postService.getPostByIdPost(post.getIdPost()))
                .thenReturn(post);

        String currentContent = post.getContentPost();
        post.setContentPost("PROMO!");

        lenient().when(postService.updatePostById(post, post.getIdPost(), account.getId()))
                .thenReturn(post);
        Post resultPost = postService.getPostByIdPost(post.getIdPost());

        assertNotEquals(currentContent, resultPost.getContentPost());
        assertEquals(post.getContentPost(), resultPost.getContentPost());
    }

    @Test
    void testServiceDeletePostByIdPost() {
        when(accountService.getAccountById(account.getId())).thenReturn(account);
        postService.createPost(post, account.getId());
        when(postService.getPostByIdPost(post.getIdPost())).thenReturn(post);

        postService.deletePostById(post.getIdPost(), account.getId());
        lenient().when(postService.getPostByIdPost(post.getIdPost())).thenReturn(null);
        assertEquals(null, postService.getPostByIdPost(post.getIdPost()));
        verify(postRepository).deleteById(post.getIdPost());
    }

    @Test
    void testGetAllPosts() {
        Iterable<Post> listPost = postRepository
                .findAllByUpdatedTimeBeforeOrderByUpdatedTimeDesc(new Date());
        lenient().when(postService.getAllPosts()).thenReturn(listPost);
        Iterable<Post> listPostResult = postService.getAllPosts();
        Assertions.assertIterableEquals(listPost, listPostResult);
    }

    @Test
    void testAllPostsContainsKeyword() {
        Iterable<Post> listPost = postRepository.findByTitlePostContainingIgnoreCase("Title");
        lenient().when(postService.getAllPostsContainsKeyword("Test")).thenReturn(listPost);
        Iterable<Post> listPostResult = postService.getAllPostsContainsKeyword("Title");
        Assertions.assertIterableEquals(listPost, listPostResult);
    }
}
