package com.adproject.cafetiere.content.service;

import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.authentication.repository.AuthRepository;
import com.adproject.cafetiere.authentication.service.AuthService;
import com.adproject.cafetiere.content.model.Comment;
import com.adproject.cafetiere.content.model.Post;
import com.adproject.cafetiere.content.repository.CommentRepository;
import com.adproject.cafetiere.content.repository.PostRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class CommentServiceImplTest {

    @Mock
    private AuthRepository accountRepository;

    @Mock
    private PostRepository postRepository;

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private AuthService accountService;

    @Mock
    private PostService postService;

    @InjectMocks
    private CommentServiceImpl commentService;

    private Post post;
    private Comment comment;
    private Account account;

    @BeforeEach
    public void setUp() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021, 1, 1, 0, 0, 0);
        Date publishedTime = calendar.getTime();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        post = new Post("TestTitle", "TestContent", publishedTime);

        calendar.set(2020, 1, 1, 1, 0, 0);
        Date publishedTime2 = calendar.getTime();
        comment = new Comment("TestIsiComment", publishedTime2);

        account = new Account("TestNamaDepan",
                "TestNamaBelakang",
                "TestEmail@mail.com",
                "TestUsername",
                "TestPassword",
                1);
        account.setId(0);
    }

    @Test
    void testServiceCreateComment() {
        when(accountService.getAccountById(account.getId())).thenReturn(account);
        when(postService.getPostByIdPost(post.getIdPost())).thenReturn(post);
        lenient().when(commentService.createComment(
                comment, account.getId(), post.getIdPost())).thenReturn(comment);
        assertEquals(commentService.createComment(
                comment, account.getId(), post.getIdPost()), comment);
        verify(commentRepository).save(comment);
    }

    @Test
    void testServiceGetCommentByCommentId() {
        lenient().when(commentService.getCommentByCommentId(comment.getIdComment()))
                .thenReturn(comment);
        assertEquals(comment, commentService.getCommentByCommentId(
                comment.getIdComment()));
    }

    @Test
    void testServiceGetCommentByPostId() {
        List<Comment> listOfComment = commentRepository
                .findCommentsByPostIdPostOrderByIdCommentDesc(post.getIdPost());
        lenient().when(commentService.getCommentByPostId(post.getIdPost()))
                .thenReturn(listOfComment);
        List<Comment> listOfCommentResult = commentService.getCommentByPostId(post.getIdPost());
        assertEquals(listOfComment, listOfCommentResult);
    }
}
