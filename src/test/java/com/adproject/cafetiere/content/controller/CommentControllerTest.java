package com.adproject.cafetiere.content.controller;

import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.authentication.service.AuthService;
import com.adproject.cafetiere.content.model.Comment;
import com.adproject.cafetiere.content.model.Post;
import com.adproject.cafetiere.content.service.CommentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CommentController.class)
class CommentControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CommentService commentService;

    @MockBean
    private AuthService authService;

    private Account account;

    private Post post;

    private Comment comment;

    @BeforeEach
    public void setUp() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021, 1, 1, 0, 0, 0);
        Date publishedTime = calendar.getTime();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        account = new Account("TestNamaDepan",
                "TestNamaBelakang",
                "TestEmail@mail.com",
                "TestUsername",
                "TestPassword",
                1);
        account.setId(0);

        post = new Post("TestTitle", "TestContent", publishedTime);

        calendar.set(2020, 1, 1, 1, 0, 0);
        Date publishedTime2 = calendar.getTime();
        comment = new Comment("TestIsiComment", publishedTime2);
        comment.setPost(post);
        post.setAdmin(account);

        List<Comment> commentList = new ArrayList<>();
        commentList.add(comment);
        post.setListOfComment(commentList);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerCreateComment() throws Exception {
        when(authService.getAccountById(anyInt())).thenReturn(account);
        when(commentService.createComment(any(Comment.class), anyInt(), anyInt()))
                .thenReturn(comment);
        String json = mapToJson(comment);

        mvc.perform(post("/comment/accountId=" + account.getId()
                + "/postId=" + post.getIdPost())
                .contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.isiComment").value(comment.getIsiComment()));
    }

    @Test
    void testControllerGetCommentsFromPost() throws Exception {
        List<Comment> commentList = new ArrayList<>();
        commentList.add(comment);

        when(commentService.getCommentByPostId(anyInt())).thenReturn(commentList);
        mvc.perform(get("/comment/postId=" + post.getIdPost()))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].isiComment").value(comment.getIsiComment()));
    }

    @Test
    void testControllerGetCommentByIdComment() throws Exception {
        when(commentService.getCommentByCommentId(anyInt())).thenReturn(comment);
        mvc.perform(get("/comment/commentId=" + comment.getIdComment())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.isiComment").value(comment.getIsiComment()));
    }

    @Test
    void testControllerDeleteComment() throws Exception {
        mvc.perform(delete("/comment/accountId=" + account.getId()
                + "/commentId=" + comment.getIdComment())
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());
    }

}
