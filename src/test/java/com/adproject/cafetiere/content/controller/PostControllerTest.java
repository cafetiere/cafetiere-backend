package com.adproject.cafetiere.content.controller;

import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.content.model.Post;
import com.adproject.cafetiere.content.service.PostService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = PostController.class)
class PostControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PostService postService;

    private Post post;

    private Account account;

    @BeforeEach
    public void setUp() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021, 1, 1, 0, 0, 0);
        Date publishedTime = calendar.getTime();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        post = new Post("TestTitle", "TestContent", publishedTime);
        account = new Account("TestNamaDepan",
                "TestNamaBelakang",
                "TestEmail@mail.com",
                "TestUsername",
                "TestPassword",
                1);
        account.setId(0);
        post.setAdmin(account);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerCreatePost() throws Exception {
        when(postService.createPost(any(Post.class), anyInt())).thenReturn(post);
        String json = mapToJson(post);

        mvc.perform(post("/post/accountId=" + account.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.titlePost").value("TestTitle"));

    }

    @Test
    void testControllerGetPostByKeyword() throws Exception {
        List<Post> listOfPost = new ArrayList<>();
        listOfPost.add(post);

        when(postService.getAllPostsContainsKeyword(anyString())).thenReturn(listOfPost);
        mvc.perform(get("/post/containTitle=" + "test")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].titlePost").value(post.getTitlePost()));
    }

    @Test
    void testControllerGetPostByIdPost() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021, 1, 2, 0, 0, 0);
        Date publishedTime = calendar.getTime();
        Post post2 = new Post("TestTitle2", "TestContent2", publishedTime);
        when(postService.getPostByIdPost(anyInt())).thenReturn(post2);
        mvc.perform(get("/post/postId=" + post2.getIdPost())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.titlePost").value("TestTitle2"));
    }


    @Test
    void testControllerUpdatePost() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021, 1, 2, 0, 0, 0);
        Date publishedTime = calendar.getTime();
        Post post2 = new Post("TestTitle2", "TestContent2", publishedTime);
        String json = mapToJson(post2);
        when(postService.updatePostById(any(Post.class), anyInt(), anyInt())).thenReturn(post2);
        mvc.perform(put("/post/accountId=" + account.getId() + "/postId=" + post2.getIdPost())
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.contentPost").value("TestContent2"));
    }

    @Test
    void testControllerDeletePost() throws Exception {
        mvc.perform(delete("/post/accountId=" + account.getId() + "/postId=" + post.getIdPost())
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());
    }


    @Test
    void testControllerGetAllPosts() throws Exception {
        List<Post> postList = new ArrayList<>();
        postList.add(post);

        when(postService.getAllPosts()).thenReturn(postList);
        mvc.perform(get("/post"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].titlePost").value(post.getTitlePost()))
                .andExpect(jsonPath("$[0].contentPost").value(post.getContentPost()));
    }
}
