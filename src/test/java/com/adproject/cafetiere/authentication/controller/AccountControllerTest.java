package com.adproject.cafetiere.authentication.controller;

import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.authentication.service.AuthService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.print.attribute.standard.Media;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = AuthController.class)
public class AccountControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AuthService authService;

    private Account account;

    @BeforeEach
    public void setUp(){
        account = new Account("nofaldi", "atmam", "nofame@email.com", "nofamex", "passwd", 1);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testAuthenticateSuccesfully() throws Exception {
        when(authService.register(account)).thenReturn(account);
        mvc.perform(post("/auth/register")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(account)))
                .andExpect(jsonPath("$.username").value(account.getUsername()));
    }

}
