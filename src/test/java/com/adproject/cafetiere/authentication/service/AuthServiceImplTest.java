package com.adproject.cafetiere.authentication.service;

import com.adproject.cafetiere.authentication.model.Account;
import com.adproject.cafetiere.authentication.repository.AuthRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthServiceImplTest {
    @Mock
    private AuthRepository authRepository;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @InjectMocks
    private AuthServiceImpl authService;

    private Account account;

    @BeforeEach
    public void setUp() {
        account = new Account("nofaldi", "atmam", "nofame@email.com", "nofamex", "passwd", 1);
        account.setId(1);
    }

    @Test
    public void testGetAccounyById() {
        when(authRepository.findAccountById(account.getId())).thenReturn(account);
        Account acc = authService.getAccountById(1);
        assertEquals(account.getUsername(), acc.getUsername());
    }

    @Test
    public void testRegisterExistedUsername() {
        when(authRepository.findAccountByUsername(account.getUsername())).thenReturn(account);
        assertThrows(RuntimeException.class, () -> {
            authService.register(account);
        });
    }

    @Test
    public void testRegisterAccount() {
        Account newAccount = new Account("test", "test2", "test@gmail.com", "tester", "testpw", 1);
        Account added = authService.register(newAccount);
        assertEquals(newAccount.getUsername(), added.getUsername());
    }
}
