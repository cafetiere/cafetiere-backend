package com.adproject.cafetiere.product.service;

import com.adproject.cafetiere.product.model.Product;
import com.adproject.cafetiere.product.repository.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductServiceImpl productService;

    private Product product;

    @BeforeEach
    public void setUp() {
        product = new Product();
        product.setId(1);
        product.setName("Latte");
        product.setDescription("ini latte");
        product.setPrice(17000);
        product.setQuantity(1);
    }

    @Test
    void testServiceCreateProduct() {
        when(productRepository.save(product)).thenReturn(product);
        Product created = productService.createProduct(product);
        assertEquals(created.getName(), product.getName());
        verify(productRepository).save(product);
    }

    @Test
    void testServiceGetListProduct() {
        Iterable<Product> listProduct = productRepository.findAll();
        lenient().when(productService.getListProduct()).thenReturn(listProduct);
        Iterable<Product> listProductResult = productService.getListProduct();
        Assertions.assertIterableEquals(listProduct, listProductResult);
    }

    @Test
    void testServiceGetProductById() {
        lenient().when(productService.getProductById(1)).thenReturn(product);
        when(productRepository.findById(1)).thenReturn(product);
        Product productResult = productService.getProductById(product.getId());
        assertEquals(product.getId(), productResult.getId());
    }

    @Test
    void testServiceDeleteProductById() {
        productService.createProduct(product);
        when(productRepository.findById(1)).thenReturn(product);
        productService.deleteProductById(1);
        lenient().when(productService.getProductById(1)).thenReturn(null);
        assertEquals(null, productService.getProductById(product.getId()));
        verify(productRepository).deleteById(1);
    }

    @Test
    void testServiceUpdateProduct() {
        productService.createProduct(product);
        when(productRepository.findById(1)).thenReturn(product);
        int currentPrice = product.getPrice();
        product.setPrice(15000);
        lenient().when(productService.updateProduct(product.getId(), product)).thenReturn(product);
        Product productResult = productService.updateProduct(product.getId(), product);
        assertNotEquals(productResult.getPrice(), currentPrice);
        assertEquals(productResult.getName(), product.getName());
    }
}
