package com.adproject.cafetiere.product.controller;

import com.adproject.cafetiere.product.model.Product;
import com.adproject.cafetiere.product.service.ProductServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ProductController.class)
class ProductControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductServiceImpl productService;

    private Product product;

    @BeforeEach
    public void setUp() {
        product = new Product("Latte", "ini latte", 17000, 1);
        product.setId(1);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerPostProduct() throws Exception {
        when(productService.createProduct(product)).thenReturn(product);
        mockMvc.perform(post("/product").contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(mapToJson(product))).andExpect(jsonPath("$.id").value(1));
    }

    @Test
    void testControllerGetListProduct() throws Exception {
        Iterable<Product> listProduct = Arrays.asList(product);
        when(productService.getListProduct()).thenReturn(listProduct);
        mockMvc.perform(get("/product").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$[0].id").value(1));
    }

    @Test
    void testControllerGetProductById() throws Exception {
        when(productService.getProductById(1)).thenReturn(product);
        mockMvc.perform(get("/product/1").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andExpect(content()
            .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.name").value("Latte"));
    }

    @Test
    void testControllerGetNonExistProduct() throws Exception {
        mockMvc.perform(get("/product/1").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    void testControllerUpdateProduct() throws Exception {
        productService.createProduct(product);
        product.setPrice(15000);
        when(productService.updateProduct(product.getId(), product)).thenReturn(product);
        mockMvc.perform(put("/product/1").contentType(MediaType.APPLICATION_JSON)
            .content(mapToJson(product))).andExpect(status().isOk()).andExpect(content()
            .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.price").value(15000));
    }

    @Test
    void testControllerDeleteProduct() throws Exception {
        productService.createProduct(product);
        mockMvc.perform(delete("/product/1").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());
    }
}
