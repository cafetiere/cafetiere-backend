# Cafetiere Application

## Status
[![coverage report](https://gitlab.com/cafetiere/cafetiere-backend/badges/staging/coverage.svg)](https://gitlab.com/cafetiere/cafetiere-backend/-/commits/staging)
[![pipeline status](https://gitlab.com/cafetiere/cafetiere-backend/badges/staging/pipeline.svg)](https://gitlab.com/cafetiere/cafetiere-backend/-/commits/staging)


## Group Member
- Nofaldi Fikrul Atmam
- Johanes Marihot Perkasa Simarmata
- Hasna Nadifah
- Piawai Said Umbara
- Ghifari Aulia Azhar Riza

## Feature
- Authentication 
- Notification
- Order / Buy Product
- CRUD Product (admin)
- Testimonials
- Post

## Production Notes
- [Database Schema](http://bit.ly/cafetiereSchema)
- [Deployed Website](https://cafetiere-backend.herokuapp.com)

## Local Development Notes
Before you start developing in local don't forget to create a new database in your postgresql.
```postgresql
CREATE DATABASE cafetiere;
```
dont forget to change postgre username and password in ``/resources/application.properties``

## Creating / Updating a Feature
- when you are working on a feature (bug, new feature, refractor, etc).
- always create a new branch with this naming conventions ``{your-name}/{branch-name}`` example ``nofaldi/refractor-auth``.

## Project Structure
- **Controller**: put all your endpoint controller here.
- **Model**: Entity class for your database.
- **Repository**: Creating database query.
- **Service**: Business logic code is here.
